# frozen_string_literal: true

module TeamMemberSelectHelper

  private

  def select_random_team_member(department)
    WwwGitLabCom.team_from_www.each_with_object([]) do |(username, data), memo|
      memo << "@#{username}" if data['departments']&.any? { |dept| dept == department }
    end.sample
  end
end
